import { loadRemoteModule } from "@angular-architects/module-federation";
import {
  Component,
  Input,
  ComponentFactoryResolver,
  Injector,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ɵcreateInjector,
} from "@angular/core";

@Component({
  selector: "federated-component",
  templateUrl: "./federated.component.html",
  styleUrls: ["./federated.component.scss"],
})
export class FederatedComponent implements OnInit {
  @ViewChild("federatedComponent", { read: ViewContainerRef })
  federatedComponent: ViewContainerRef | any;
  @Input() remoteEntry: string | any;
  @Input() remoteName: string | any;
  @Input() exposedModule: string | any;
  @Input() componentName: string | any;

  constructor(
    private injector: Injector,
    private cfr: ComponentFactoryResolver
  ) {}
  ngOnInit(): void {
    loadRemoteModule({
      remoteEntry: this.remoteEntry,
      remoteName: this.remoteName,
      exposedModule: this.exposedModule,
    }).then((federated) => {
      const componentFactory = this.cfr.resolveComponentFactory(
        federated[this.exposedModule].ɵmod.exports.find(
          (e:any) => e.name === this.componentName
        )
      );
      this.federatedComponent.createComponent(
        componentFactory,
        0,
        ɵcreateInjector(federated[this.exposedModule], this.injector)
      );
    });
  }
}
