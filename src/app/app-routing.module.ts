import { loadRemoteModule } from '@angular-architects/module-federation';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {
    path: '',
    component:HomeComponent
  },
  {
    path: 'mfe1',
    loadChildren: () =>
      loadRemoteModule({
        remoteName: "mfe1",
        remoteEntry: "http://localhost:4300/remoteEntry.js",
        exposedModule: "Mfe1Module",
      }).then((m) => m.Mfe1Module),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
